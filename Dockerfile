FROM josh5/base-ubuntu:16.04
LABEL maintainer="Josh.5 <jsunnex@gmail.com>"


ARG OWNCLOUD_VERSION=10.2.1

### Install pyinotify service.
RUN \
    echo "**** Update sources ****" \
        && apt-get update \
    && \
    echo "**** Install dependencies ****" \
        && apt-get install -y \
            bzip2 \
    && \
    echo "**** Install sqlite ****" \
        && apt-get install -y \
            sqlite \
    && \
    echo "**** Install php ****" \
        && apt-get install -y \
            php-gd \
            php-json \
            php-mysql \
            php-sqlite3 \
            php-pgsql \
            php-curl \
            php-intl \
            php-mcrypt \
            php-imagick \
            php-zip \
            php-xml \
            php-mbstring \
            php-soap \
            php-ldap \
            php-apcu \
            php-redis \
            php-smbclient \
            php-gmp \
    && \
    echo "**** Install apache ****" \
        && apt-get install -y \
            apache2 \
            libapache2-mod-php \
    && \
    echo "**** Cleanup ****" \
        && apt-get purge -y --auto-remove \
	        ${BUILD_DEPENDENCIES} \
        && apt-get clean \
        && rm -rf \
            /tmp/* \
            /var/tmp/* \
            /etc/apache2/envvars \
            /etc/apache2/conf-* \
            /etc/apache2/sites-* \
            /var/log/apache2/* \
            /var/lib/apt/lists/* \
    && \
    echo "**** Configure apache ****" \
        && a2enmod \
            rewrite \
            headers \
            env \
            dir \
            mime \
            expires \
            remoteip \
        && mkdir -p /var/www/html  \
        && chown -R www-data:www-data /var/www/html /var/log/apache2 /var/run/apache2 \
        && chsh -s /bin/bash www-data \
        && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && \
    echo
# TODO: Check if composer is required.. Rather not have it

# Install apache2 s6 overlay init
ARG S6_INIT_SCRIPTS_VERSION="v0.12"
ARG S6_INIT_PACKAGES="apache2"
RUN \
    echo "**** Install init process ****" \
        && curl -L "https://github.com/josh5/s6-init/archive/${S6_INIT_SCRIPTS_VERSION}.tar.gz" -o /tmp/s6-init.tar.gz \
        && tar xfz /tmp/s6-init.tar.gz -C /tmp \
        && cd /tmp/s6-init-* \
        && ./install \
    && \
    echo

# Install owncloud from tarball
RUN \
    echo "**** Download owncloud ****" \
        && curl -L \
            https://download.owncloud.org/community/owncloud-${OWNCLOUD_VERSION}.tar.bz2 \
            -o /tmp/owncloud.tar.bz2 \
    && \
    echo 
RUN \
    echo "**** Extract owncloud ****" \
        && mkdir -p /app/www \
        && tar xf /tmp/owncloud.tar.bz2 -C /app/www/ \
        && chown -R docker:docker /app/www/owncloud/ \
    && \
    echo
RUN \
    echo "**** Cleanup ****" \
        && rm -rf \
            /tmp/* \
    && \
    echo



### Add local files
COPY /overlay /


### Intended ports and volumes
VOLUME \
    /library \
    /cache